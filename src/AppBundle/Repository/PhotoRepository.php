<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * PhotoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PhotoRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastPostedPhotoByUser(User $user)
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.user', 'u')
            ->where('u = :user')
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('user', $user)
            ->select('p')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
