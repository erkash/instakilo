<?php

namespace AppBundle\Service;


use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class Mailer
{
    const TEMPLATE = '@App/Email/new-post.html.twig';

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $engine;

    private $message;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $engine)
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
    }

    public function create(Photo $photo, User $author)
    {
        $deliveryAddresses =[];

        /** @var User $follower */
        foreach ($author->getFollowers() as $follower) {
            $deliveryAddresses[] = $follower->getEmail();
        }

        $body = $this->engine->render(self::TEMPLATE,[
            'author' => $author,
            'photo'  => $photo
        ]);

        $this->message = \Swift_Message::newInstance()
            ->setTo($deliveryAddresses)
            ->setFrom('testirovshik88@gmail.com')
            ->setBody($body, 'text/html')
            ->setSubject('New Post')
        ;

        return $this;
    }

    public function send()
    {
        $this->mailer->send($this->message);
    }
}