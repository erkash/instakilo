<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Form\FollowType;
use AppBundle\Form\UnsubscribeType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @Route("/users", name="all_users")
     */
    public function allUsers()
    {
        $currentUser = $this->getUser();
        $users       = $this->getDoctrine()
                            ->getRepository('AppBundle:User')
                            ->findAll();

        $followForm = [];
        /** @var User $user */
        foreach ($users as $user) {
            if (!$user->getFollowers()->contains($this->getUser())) {
                $followForm[$user->getId()] = $this->createForm(FollowType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_subscribe', [
                        'id' => $user->getId()
                    ])
                ])->createView();
            } else {
                $followForm[$user->getId()] = $this->createForm(UnsubscribeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_unsubscribe', [
                        'id' => $user->getId()
                    ])
                ])->createView();
            }
        }

        return $this->render('@App/User/users.html.twig', [
            'users'        => $users,
            'follow_form'  => $followForm,
            'current_user' => $currentUser
        ]);
    }

    /**
     * @Method("GET")
     * @Route("/user/{id}")
     * @param User $profile
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(User $profile)
    {
        if (!$this->getUser())  return $this->redirectToRoute('fos_user_security_login');

        $followings  = count($profile->getFollowings());
        $followers   = count($profile->getFollowers());
        $posts       = $profile->getPhotos();
        $count_posts = 0;
        $count       = count($posts);

        for ($i = 0; $i < $count; $i++)
            $count_posts++;

        return $this->render('@App/User/profile.html.twig', [
            'profile'     => $profile,
            'followings'  => $followings,
            'followers'   => $followers,
            'count_posts' => $count_posts,
            'posts'       => $posts
        ]);
    }

    /**
     * @Method("POST")
     * @Route("/subscribe/{id}")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function subscribeAction(User $user)
    {
        $follower = $this->getUser();

        if (!$follower)  return $this->redirectToRoute('fos_user_security_login');

        $user->addFollower($follower);
        $follower->addFollowing($user);

        $this->em->persist($user);
        $this->em->persist($follower);
        $this->em->flush();

        return $this->redirectToRoute('all_users');
    }

    /**
     * @Method("POST")
     * @Route("/unsubscribe/{id}")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function unSubscribeAction(User $user)
    {
        $follower = $this->getUser();

        if (!$follower)  return $this->redirectToRoute('fos_user_security_login');

        $user->removeFollower($follower);
        $follower->removeFollowing($user);

        $this->em->persist($user);
        $this->em->persist($follower);
        $this->em->flush();

        return $this->redirectToRoute('all_users');
    }

    /**
     * @Method("POST")
     * @Route("/like/{id}")
     * @param Photo $photo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function likeAction(Photo $photo)
    {
        $user = $this->getUser();

        if (!$user)  return $this->redirectToRoute('fos_user_security_login');

        $photo->addLikeUser($user);
        $user->addLikePhoto($photo);

        $this->em->persist($photo);
        $this->em->persist($user);
        $this->em->flush();

        return $this->redirectToRoute('app_site_feed');
    }

    /**
     * @Method("POST")
     * @Route("/unlike/{id}")
     * @param Photo $photo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function unLikeAction(Photo $photo)
    {
        $user = $this->getUser();

        if (!$user)  return $this->redirectToRoute('fos_user_security_login');

        $photo->removeLikeUser($user);
        $user->removeLikePhoto($photo);

        $this->em->persist($photo);
        $this->em->flush();

        return $this->redirectToRoute('app_site_feed');
    }

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}
