<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Photo;
use AppBundle\Form\CommentType;
use AppBundle\Form\UnlikeType;
use AppBundle\Form\LikeType;
use AppBundle\Form\PhotoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        if ($this->getUser()) return $this->redirectToRoute('app_site_feed');

        return $this->render('AppBundle:Site:index.html.twig');
    }

    /**
     * @Route("/feed")
     * @Security("has_role('ROLE_USER')")
     */
    public function feedAction()
    {
        $photos = $this->getDoctrine()
            ->getRepository('AppBundle:Photo')
            ->findAll();

        $photoForm    = $this->createForm(PhotoType::class);
        $commentForm  = $this->createForm(CommentType::class);
        $likeForm     = [];
        $likesCount   = [];
          /** @var Photo $photo */
        foreach ($photos as $photo) {
            $likesCount[$photo->getId()] = count($photo->getLikeUsers());
            if (!$photo->getLikeUsers()->contains($this->getUser())) {
                $likeForm[$photo->getId()] = $this->createForm(LikeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_like', [
                        'id' => $photo->getId()
                    ])
                ])->createView();
            } else {
                $likeForm[$photo->getId()] = $this->createForm(UnlikeType::class, null, [
                    'method' => 'POST',
                    'action' => $this->generateUrl('app_user_unlike', [
                        'id' => $photo->getId()
                    ])
                ])->createView();
            }
        }

        return $this->render('@App/Site/feed.html.twig', [
            'photoForm'    => $photoForm->createView(),
            'photos'       => $photos,
            'user'         => $this->getUser(),
            'comment_form' => $commentForm->createView(),
            'likeForm'     => $likeForm,
            'likesCount'   => $likesCount
        ]);
    }

    /**
     * @Method({"POST", "GET"})
     * @Route("/new")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_USER')")
     */
    public function newAction(Request $request)
    {
        $photo = new Photo();
        $form  = $this->createForm(PhotoType::class, $photo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $photo->setUser($this->getUser());

            $em = $this->getDoctrine()->getManager();
            $em->persist($photo);
            $em->flush();

            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->addListenerService('kernel.terminate', ['notification.mail.subscriber', 'onTerminate']);
        }

        return $this->redirectToRoute('app_site_feed');
    }
}
