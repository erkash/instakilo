<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Photo;
use AppBundle\Form\CommentType;
use AppBundle\Form\LikeType;
use AppBundle\Form\UnlikeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PhotoController extends Controller
{
    /**
     * @Route("/photo/{id}")
     * @param Photo $photo
     * @return \Symfony\Component\HttpFoundation\Response
     * @Security("has_role('ROLE_USER')")
     */
    public function photoDetailAction(Photo $photo)
    {
        $likesCount  = $photo->getLikeUsers()->count();
        $commentForm = $this->createForm(CommentType::class);

        if (!$photo->getLikeUsers()->contains($this->getUser())) {
            $likeForm = $this->createForm(LikeType::class, null, [
                'method' => 'POST',
                'action' => $this->generateUrl('app_user_like', [
                    'id' => $photo->getId()
                ])
            ]);
        } else {
            $likeForm = $this->createForm(UnlikeType::class, null, [
                'method' => 'POST',
                'action' => $this->generateUrl('app_user_unlike', [
                    'id' => $photo->getId()
                ])
            ]);
        }

        return $this->render('@App/Photo/detail.html.twig', [
            'photo'        => $photo,
            'likesCount'   => $likesCount,
            'likeForm'     => $likeForm->createView(),
            'comment_form' => $commentForm->createView()
        ]);
    }
}