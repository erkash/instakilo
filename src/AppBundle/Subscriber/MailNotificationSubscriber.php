<?php

namespace AppBundle\Subscriber;


use AppBundle\Entity\Photo;
use AppBundle\Entity\User;
use AppBundle\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MailNotificationSubscriber
{
    /**
     * @var Mailer
     */
    private $mailer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(Mailer $mailer, EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->mailer        = $mailer;
        $this->entityManager = $entityManager;
        $this->tokenStorage  = $tokenStorage;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onTerminate()
    {
        /** @var User $author */
        $author = $this->tokenStorage->getToken()->getUser();
        /** @var Photo $photo */
        $photo  = $this->entityManager->getRepository('AppBundle:Photo')->getLastPostedPhotoByUser($author);
        $this->mailer->create($photo, $author)->send();
    }
}