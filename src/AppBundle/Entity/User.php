<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="user")
     */
    private $photos;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $name;


    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="followings")
     */
    protected $followers;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers")
     * @ORM\JoinTable(name="subscriber",
     *      joinColumns={@ORM\JoinColumn(name="following_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="follower_id", referencedColumnName="id")}
     *      )
     */
    protected $followings;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    protected $avatar;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Photo", mappedBy="like_users")
     */
    private $likePhotos;

    public function __construct()
    {
        parent::__construct();
        $this->followers  = new ArrayCollection();
        $this->followings = new ArrayCollection();
        $this->photos     = new ArrayCollection();
        $this->likePhotos = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * get avatar
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     *
     * @param File|null $imageFile
     * @return User
     */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\User $follower
     *
     * @return User
     */
    public function addFollower(User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\User $follower
     */
    public function removeFollower(User $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following
     *
     * @param \AppBundle\Entity\User $following
     *
     * @return User
     */
    public function addFollowing(User $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \AppBundle\Entity\User $following
     */
    public function removeFollowing(User $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * Add likePhoto
     *
     * @param \AppBundle\Entity\Photo $likePhoto
     *
     * @return User
     */
    public function addLikePhoto(Photo $likePhoto)
    {
        $this->likePhotos[] = $likePhoto;

        return $this;
    }

    /**
     * Remove likePhoto
     *
     * @param \AppBundle\Entity\Photo $likePhoto
     */
    public function removeLikePhoto(Photo $likePhoto)
    {
        $this->likePhotos->removeElement($likePhoto);
    }

    /**
     * Get likePhotos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikePhotos()
    {
        return $this->likePhotos;
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return User
     */
    public function addPhoto(Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhotos()
    {
        return $this->photos;
    }
}
